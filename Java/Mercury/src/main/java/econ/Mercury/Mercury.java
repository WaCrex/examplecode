package econ.Mercury;

/**
 * Copyright 2013 Patrick "WaCrex" Linerudt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.util.logging.Logger;

/* Vault Imports */
import net.milkbowl.vault.chat.Chat;
import net.milkbowl.vault.permission.Permission;

/* Bukkit Imports */
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * @author WaCrex
 */
public class Mercury extends JavaPlugin {

    public String
        PLUGIN_NAME         = null,
        PLUGIN_VERSION      = null;

    private Logger
        PLUGIN_LOGGER       = null;

    public static Permission
        VAULT_PERMISSION    = null;

    public static Chat
        VAULT_CHAT          = null;

    @Override
    public void onDisable() {
        this.PLUGIN_LOGGER.info("Version " + this.PLUGIN_VERSION + " disabled.");
        
        /* Clean up */
        this.PLUGIN_LOGGER  = null;
        this.PLUGIN_VERSION = null;
    }

    @Override
    public void onEnable() {
        this.PLUGIN_LOGGER  = this.getLogger();
        this.PLUGIN_VERSION = this.getDescription().getVersion();

        /* Hook into Vault's Permission system */
        if(!setupPermissions()) {
            this.PLUGIN_LOGGER.severe("Failed to hook into Vault's Permission.class, please make sure what you have Vault on the server.");
            Bukkit.getPluginManager().disablePlugin(this);
        }
        
        /* Hook into Vault's Chat system */
        if(!setupChat()) {
            this.PLUGIN_LOGGER.severe("Failed to hook into Vault's Chat.class, please make sure what you have Vault on the server.");
            Bukkit.getPluginManager().disablePlugin(this);
        }

        
    }
    
    private boolean setupPermissions() {
        RegisteredServiceProvider<Permission> permissionProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.permission.Permission.class);
        if (permissionProvider != null) {
            VAULT_PERMISSION = permissionProvider.getProvider();
        }
        return (VAULT_PERMISSION != null);
    }

    private boolean setupChat() {
        RegisteredServiceProvider<Chat> chatProvider = getServer().getServicesManager().getRegistration(net.milkbowl.vault.chat.Chat.class);
        if (chatProvider != null) {
            VAULT_CHAT = chatProvider.getProvider();
        }
        return (VAULT_CHAT != null);
    }
}
