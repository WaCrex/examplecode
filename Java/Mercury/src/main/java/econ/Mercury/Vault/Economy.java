package econ.Mercury.Vault;

import java.util.List;

import net.milkbowl.vault.economy.EconomyResponse;
import net.milkbowl.vault.economy.EconomyResponse.ResponseType;
import econ.Mercury.Core.Econ;
import econ.Mercury.Core.TransactionResult;

public class Economy implements net.milkbowl.vault.economy.Economy {
    
    protected final Econ eco;
    
    public Economy(Econ eco) {
        this.eco = eco;
    }

    public boolean isEnabled() {
        return this.eco.plugin.isEnabled();
    }

    public String getName() {
        return this.eco.plugin.PLUGIN_NAME;
    }

    public boolean hasBankSupport() {
        return false;
    }

    public int fractionalDigits() {
        return 0;
    }

    public String format(double paramDouble) {
        // TODO Auto-generated method stub
        return null;
    }

    public String currencyNamePlural() {
        // TODO Implement This
        return "Gold";
    }

    public String currencyNameSingular() {
        // TODO Implement This
        return "Gold";
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#hasAccount(String)}
     */
    public boolean hasAccount(String playerName) {
        return this.eco.hasAccount(playerName);
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#hasAccount(String, String)}
     */
    public boolean hasAccount(String playerName, String worldName) {
        return this.eco.hasAccount(playerName, worldName);
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#getHoldings(String)}
     */
    public double getBalance(String playerName) {
        return this.eco.getHoldings(playerName);
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#getHoldings(String, String)}
     */
    public double getBalance(String playerName, String worldName) {
        return this.eco.getHoldings(playerName, worldName);
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#has(String, Double)}
     */
    public boolean has(String playerName, double amount) {
        return this.eco.has(playerName, amount);
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#has(String, String, Double)}
     */
    public boolean has(String playerName, String worldName, double amount) {
        return this.eco.has(playerName, worldName, amount);
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#withdrawPlayer(String, Double)}
     */
    public EconomyResponse withdrawPlayer(String playerName, double amount) {
        TransactionResult tr = this.eco.withdrawPlayer(playerName, amount);
        
        switch(tr.type.getId()) {
            case 1: return new EconomyResponse(tr.amount, tr.balance, ResponseType.SUCCESS, tr.message);
            default: return new EconomyResponse(tr.amount, tr.balance, ResponseType.FAILURE, tr.message);
        }
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#withdrawPlayer(String, String, Double)}
     */
    public EconomyResponse withdrawPlayer(String playerName, String worldName, double amount) {
        TransactionResult tr = this.eco.withdrawPlayer(playerName, worldName, amount);
        
        switch(tr.type.getId()) {
            case 1: return new EconomyResponse(tr.amount, tr.balance, ResponseType.SUCCESS, tr.message);
            default: return new EconomyResponse(tr.amount, tr.balance, ResponseType.FAILURE, tr.message);
        }
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#depositPlayer(String, Double)}
     */
    public EconomyResponse depositPlayer(String playerName, double amount){
        TransactionResult tr = this.eco.depositPlayer(playerName, amount);
        
        switch(tr.type.getId()) {
            case 1: return new EconomyResponse(tr.amount, tr.balance, ResponseType.SUCCESS, tr.message);
            default: return new EconomyResponse(tr.amount, tr.balance, ResponseType.FAILURE, tr.message);
        }
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#depositPlayer(String, String, Double)}
     */
    public EconomyResponse depositPlayer(String playerName, String worldName, double amount) {
        TransactionResult tr = this.eco.depositPlayer(playerName, worldName, amount);
        
        switch(tr.type.getId()) {
            case 1: return new EconomyResponse(tr.amount, tr.balance, ResponseType.SUCCESS, tr.message);
            default: return new EconomyResponse(tr.amount, tr.balance, ResponseType.FAILURE, tr.message);
        }
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#createBank(String, String)}
     */
    public EconomyResponse createBank(String name, String player) {
        TransactionResult tr = this.eco.createBankAccount(name, player);
        
        switch(tr.type.getId()) {
            case 1: return new EconomyResponse(tr.amount, tr.balance, ResponseType.SUCCESS, tr.message);
            default: return new EconomyResponse(tr.amount, tr.balance, ResponseType.FAILURE, tr.message);
        }
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#deleteBank(String)}
     */
    public EconomyResponse deleteBank(String name) {
        TransactionResult tr = this.eco.deleteBankAccount(name);
        
        switch(tr.type.getId()) {
            case 1: return new EconomyResponse(tr.amount, tr.balance, ResponseType.SUCCESS, tr.message);
            default: return new EconomyResponse(tr.amount, tr.balance, ResponseType.FAILURE, tr.message);
        }
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#bankHoldings(String)}
     */
    public EconomyResponse bankBalance(String name) {
        TransactionResult tr = this.eco.bankHoldings(name);
        
        switch(tr.type.getId()) {
            case 1: return new EconomyResponse(tr.amount, tr.balance, ResponseType.SUCCESS, tr.message);
            default: return new EconomyResponse(tr.amount, tr.balance, ResponseType.FAILURE, tr.message);
        }
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#bankHas(String, Double)}
     */
    public EconomyResponse bankHas(String name, double amount) {
        TransactionResult tr = this.eco.bankHas(name, amount);
        
        switch(tr.type.getId()) {
            case 1: return new EconomyResponse(tr.amount, tr.balance, ResponseType.SUCCESS, tr.message);
            default: return new EconomyResponse(tr.amount, tr.balance, ResponseType.FAILURE, tr.message);
        }
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#bankWithdraw(String, Double)}
     */
    public EconomyResponse bankWithdraw(String accountName, double amount) {
        TransactionResult tr = this.eco.bankWithdraw(accountName, amount);
        
        switch(tr.type.getId()) {
            case 1: return new EconomyResponse(tr.amount, tr.balance, ResponseType.SUCCESS, tr.message);
            default: return new EconomyResponse(tr.amount, tr.balance, ResponseType.FAILURE, tr.message);
        }
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#bankDeposit(String, Double)}
     */
    public EconomyResponse bankDeposit(String accountName, double amount) {
        TransactionResult tr = this.eco.bankDeposit(accountName, amount);
        
        switch(tr.type.getId()) {
            case 1: return new EconomyResponse(tr.amount, tr.balance, ResponseType.SUCCESS, tr.message);
            default: return new EconomyResponse(tr.amount, tr.balance, ResponseType.FAILURE, tr.message);
        }
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#isBankOwner(String, String)}
     */
    public EconomyResponse isBankOwner(String accountName, String playerName) {
        TransactionResult tr = this.eco.isBankOwner(accountName, playerName);
        
        switch(tr.type.getId()) {
            case 1: return new EconomyResponse(tr.amount, tr.balance, ResponseType.SUCCESS, tr.message);
            default: return new EconomyResponse(tr.amount, tr.balance, ResponseType.FAILURE, tr.message);
        }
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#isBankMember(String, String)}
     */
    public EconomyResponse isBankMember(String accountName, String playerName) {
        TransactionResult tr = this.eco.isBankMember(accountName, playerName);
        
        switch(tr.type.getId()) {
            case 1: return new EconomyResponse(tr.amount, tr.balance, ResponseType.SUCCESS, tr.message);
            default: return new EconomyResponse(tr.amount, tr.balance, ResponseType.FAILURE, tr.message);
        }
    }

    /**
     * Proxy for {@link econ.Mercury.Core.Econ#getBanks()}
     */
    public List<String> getBanks() {
        return this.eco.getBanks();
    }

    public boolean createPlayerAccount(String playerName) {
        // TODO Implement This
        return hasAccount(playerName);
    }

    public boolean createPlayerAccount(String playerName, String world) {
        // TODO Implement This
        return hasAccount(playerName);
    }
}
