package econ.Mercury.Core;


public class Transaction {

    protected final Integer references;
    protected final String recipient;
    protected final String account;
    protected final Double Amount;
    protected final Long date = System.currentTimeMillis();

    public Transaction (String recipient, String accountName, Double amount, Econ eco) {
        this.references = eco.ReferencesID();
        this.recipient  = recipient;
        this.account    = accountName;
        this.Amount     = amount;
    }

    public Integer getReferencesID() {
        return this.references;
    }

    public String getRecipoent() {
        return this.recipient;
    }

    public String getAccount() {
        return this.account;
    }

    public Double getAmount() {
        return this.Amount;
    }

    public Long getDate() {
        return this.date;
    }
}
