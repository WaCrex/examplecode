package econ.Mercury.Core;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;

import econ.Mercury.Mercury;
import econ.Mercury.Core.TransactionResult.Type;

public class Econ {
    
    public final Mercury plugin;
    
    protected Integer referencesCounter;
    
    protected Map<String, List<PlayerAccount>> playerAccounts;
    protected Map<String, BankAccount> bankAccounts;

    public Integer ReferencesID() {
        return this.referencesCounter++;
    }
    
    public Econ(Mercury plugin) {
        this.plugin = plugin;
    }

    /**
     * Checks if this player has an account on the server yet
     * This will always return true if the player has joined the server at least once
     * @param playerName     Name of the player
     * @return               If the player has an account
     */
     public boolean hasAccount(String playerName) {
         /* Search for accounts with player as owner */
         for(List<PlayerAccount> accounts : this.playerAccounts.values()) {
             for(PlayerAccount account : accounts) {
                 /* Check if player is the account owner */
                 if(account.getOwner() == playerName)
                     return true;
             }
         }
         return false;
     }

     /**
     * Checks if this player has an account on the server yet on the given world
     * This will always return true if the player has joined the server at least once
     * @param playerName     Name of the player
     * @param worldName      Name of the world
     * @return               If the player has an account
     */
     public boolean hasAccount(String playerName, String worldName) {
         /* Check if there's a world by that name */
         if(!this.playerAccounts.containsKey(worldName))
             return false;

         /* Search for accounts with player as owner */
         for(PlayerAccount account : this.playerAccounts.get(worldName)) {
             /* Check if player is the account owner */
             if(account.getOwner() == playerName)
                 return true;
         }
         return false;
     }

    /**
     * Gets holdings of a player
     * @param playerName    Name of the player
     * @return              Amount currently held in players account
     */
    public Double getHoldings(String playerName) {
        /* Get the name of the player's current world */
        String worldName = Bukkit.getServer().getPlayer(playerName).getWorld().getName();

        /* Gets holdings of a player on the specified world. */
        return getHoldings(playerName, worldName);
    }

    /**
     * Gets holdings of a player on the specified world.
     * @param playerName    Name of the player
     * @param name          Name of the world
     * @return              Amount currently held in players account
     */
    public Double getHoldings(String playerName, String worldName) {
        /* Check if there's a world by that name */
        if(!this.bankAccounts.containsKey(worldName))
            return 0.0D;
        
        Double holdings = 0.0D;
        
        /* Search for accounts with player as owner */
        for(PlayerAccount account : this.playerAccounts.get(worldName)) {
            /* Check if player is the account owner */
            if(account.getOwner() == playerName)
                holdings += account.getHoldings();
        }
        return holdings;
    }

    /**
     * Checks if the player account has the amount - DO NOT USE NEGATIVE AMOUNTS
     *
     * @param playerName    Name of the player
     * @param amount
     * @return True if <b>playerName</b> has <b>amount</b>, False else wise
     */
    public boolean has(String playerName, Double amount) {
        return this.getHoldings(playerName) >= amount;
    }

    /**
     * Checks if the player account has the amount in a given world - DO NOT USE NEGATIVE AMOUNTS
     * @param playerName    Name of the player
     * @param worldName     Name of the world
     * @param amount
     * @return True if <b>playerName</b> has <b>amount</b>, False else wise
     */
    public boolean has(String playerName, String worldName, Double amount) {
        return this.getHoldings(playerName, worldName) >= amount;
    }
    
    /**
     * Withdraw an amount from a player - DO NOT USE NEGATIVE AMOUNTS
     * @param playerName    Name of player
     * @param amount        Amount to withdraw
     * @return              Detailed response of transaction
     */
    public TransactionResult withdrawPlayer(String playerName, Double amount) {
        /* Get the name of the player's current world */
        String worldName = Bukkit.getServer().getPlayer(playerName).getWorld().getName();
        
        /* Check world has any player accounts */
        if(!this.playerAccounts.containsKey(worldName))
            return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "The world '" + worldName + "' doesn't have any player accounts.");

        /* Withdraw an amount from a player on a given world */
        return this.withdrawPlayer(playerName, worldName, amount);
    }
    
    /**
     * Withdraw an amount from a player on a given world - DO NOT USE NEGATIVE AMOUNTS
     * @param playerName    Name of player
     * @param worldName     Name of the world
     * @param amount        Amount to withdraw
     * @return              Detailed response of transaction
     */
    public TransactionResult withdrawPlayer(String playerName, String worldName, Double amount) {
        /* Check world has any player accounts */
        if(!this.playerAccounts.containsKey(worldName))
            return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "The world '" + worldName + "' doesn't have any player accounts.");
        
        List<PlayerAccount> accounts = this.playerAccounts.get(worldName);

        /* Search for player account owned by player */
        for(int i=0; i<accounts.size(); i++) {
            if(accounts.get(i).getOwner() == playerName) {
                PlayerAccount account = accounts.get(i);
            
                /* Withdraw the money if the bank has enough */
                if(account.withdraw(amount)) {
                    /* Add account to list */
                    accounts.set(i, account);
                    
                    /* Update account list */
                    this.playerAccounts.put(worldName, accounts);
                    
                    return new TransactionResult(-amount, account.getHoldings(), Type.SUCCESS, "'" + "' was successfully withdrawn from the player account."); //TODO fix currency names
                } /* Didn't have enough */
                return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "The player account doesn't have enough holdings.");
            }
        } /* Couldn't find any account */
        return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "The player account doesn't exist.");
    }

    /**
    * Deposit an amount to a player - DO NOT USE NEGATIVE AMOUNTS
    * @param playerName     Name of player
    * @param amount         Amount to deposit
    * @return               Detailed response of transaction
    */
    public TransactionResult depositPlayer(String playerName, Double amount){
        /* Get the name of the player's current world */
        String worldName = Bukkit.getServer().getPlayer(playerName).getWorld().getName();

        /* Check world has any player accounts */
        if(!this.playerAccounts.containsKey(worldName))
            return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "The world '" + worldName + "' doesn't have any player accounts.");

        /* Deposit an amount to the player */
        return this.depositPlayer(playerName, worldName, amount);
    }
    
    /**
    * Deposit an amount to a player - DO NOT USE NEGATIVE AMOUNTS
    * @param playerName     Name of player
    * @param worldName      Name of the world
    * @param amount         Amount to deposit
    * @return               Detailed response of transaction
    */
    public TransactionResult depositPlayer(String playerName, String worldName, Double amount){
        /* Check world has any player accounts */
        if(!this.playerAccounts.containsKey(worldName))
            return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "The world '" + worldName + "' doesn't have any player accounts.");

        List<PlayerAccount> accounts = this.playerAccounts.get(worldName);

        /* Search for player account owned by player */
        for(int i=0; i<accounts.size(); i++) {
            if(accounts.get(i).getOwner() == playerName) {
                PlayerAccount account = accounts.get(i);
                
                /* Withdraw the money if the bank has enough */
                account.deposit(amount);
                
                /* Check if it was a success */
                if(account.getHoldings() == accounts.get(i).getHoldings() + amount) {
                    /* Add account to list */
                    accounts.set(i, account);
                            
                    /* Update account list */
                    this.playerAccounts.put(worldName, accounts);

                    return new TransactionResult(amount, account.getHoldings(), Type.SUCCESS, "'" + "' was successfully deposited to the player account."); //TODO fix currency names
                } /* Unknown error */
                return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "An error occured while depositing money to the player account.");
            }
        } /* Couldn't find any account */
        return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "The player account doesn't exist.");
    }

    /**
     * Creates a bank account with the specified name and the player as the owner
     * @param accountName   Name of the bank account
     * @param playerName    Name of the player
     * @return              TransactionResult Object
     */
    public TransactionResult createBankAccount(String accountName, String playerName) {
        /* Check if there already is a bank by that name */
        if(this.bankAccounts.containsKey(accountName))
            return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "There is already a bank account by the name '" + accountName + "'.");
        
        Map<String, BankAccount> newList = this.bankAccounts;
        
        /* Try to create bank account */
        newList.put(accountName, new BankAccount(playerName));
        
        /* Check if it was a success */
        if(newList.size() != this.bankAccounts.size()) {
            /* Update bank list */
            this.bankAccounts = newList;
            return new TransactionResult(0.0D, 0.0D, Type.SUCCESS, "Successfuly created the bank account '" + accountName + "' with '" + playerName + "' as owner.");
        }
        return new TransactionResult(0.0D, 0.0D, Type.ERROR, "An error occured while creating the bank '" + accountName + "' with '" + playerName + "' as owner.");
    }

    /**
     * Deletes a bank account with the specified name.
     * @param accountName   Name of the bank account
     * @return              TransactionResult Object
     */
    public TransactionResult deleteBankAccount(String accountName) {
        /* Check if there's a bank by that name */
        if(!this.bankAccounts.containsKey(accountName))
            return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "There is no bank account by the name '" + accountName + "'.");
        
        Map<String, BankAccount> newList = this.bankAccounts;

        /* Try to remove the bank  */
        newList.remove(accountName);
        
        /* Check if it was a success */
        if(newList.size() != this.bankAccounts.size()) {
            /* Update bank list */
            this.bankAccounts = newList;
            return new TransactionResult(0.0D, 0.0D, Type.SUCCESS, "The bank account " + accountName + "' was successfuly deleted.");
        } /* Unknown error */ 
        return new TransactionResult(0.0D, 0.0D, Type.ERROR, "Could not delete the bank account '" + accountName + "'.");
    }

    /**
     * Returns the amount the bank account has
     * @param accountName   Name of the bank account
     * @return              TransactionResult Object
     */
    public TransactionResult bankHoldings(String accountName) {
        /* Check if there's a bank by that name */
        if(!this.bankAccounts.containsKey(accountName))
            return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "There is no bank account by the name '" + accountName + "'.");

        /* Return balance if found */
        return new TransactionResult(0.0D, this.bankAccounts.get(accountName).getHoldings(), Type.SUCCESS, "Succefully obtained the holdings of the bank account '" + accountName + "'.");
    }
    
    /**
     * Returns true or false whether the bank account has the amount specified - DO NOT USE NEGATIVE AMOUNTS
     * @param accountName    Name of the bank account
     * @param amount
     * @return               TransactionResult Object
     */
    public TransactionResult bankHas(String accountName, Double amount) {
        /* Check if there's a bank by that name */
        if(!this.bankAccounts.containsKey(accountName))
            return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "There is no bank account by the name '" + accountName + "'.");

        /* Check if there's enough */
        if(this.bankAccounts.get(accountName).getHoldings() >= amount)
            return new TransactionResult(0.0D, 0.0D, Type.SUCCESS, "The bank account '" + accountName + "' have got enough holdings.");
        else
            return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "The bank account '" + accountName + "' doesn't have enough holdings.");
    }

    /**
     * Withdraw an amount from a bank account - DO NOT USE NEGATIVE AMOUNTS
     * @param accountName   Name of the bank account
     * @param amount
     * @return              TransactionResult Object
     */
    public TransactionResult bankWithdraw(String accountName, Double amount) {
        /* Check if there's a bank by that name */
        if(!this.bankAccounts.containsKey(accountName))
            return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "There is no bank account by the name '" + accountName + "'.");
        
        /* Get the bank from the list  */
        BankAccount account = this.bankAccounts.get(accountName);
        
        /* Withdraw the money if the bank has enough */
        if(account.withdraw(amount)) {
            /* Update bank list */
            this.bankAccounts.put(accountName, account);
            return new TransactionResult(-amount, account.getHoldings(), Type.SUCCESS, "'" + "' was successfully withdrawn from the bank account '" + accountName + "'.");
        } /* Didn't have enough */
        return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "The bank account '" + accountName + "' doesn't have enough holdings.");
    }
    
    /**
     * Deposit an amount into a bank account - DO NOT USE NEGATIVE AMOUNTS
     * @param accountName   Name of the bank account
     * @param amount
     * @return              TransactionResult Object
     */
    public TransactionResult bankDeposit(String accountName, Double amount) {
        /* Check if there's a bank by that name */
        if(!this.bankAccounts.containsKey(accountName))
            return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "There is no bank account by the name '" + accountName + "'.");

        /* Get the bank from the list  */
        BankAccount oldAccount = this.bankAccounts.get(accountName);
        BankAccount account = this.bankAccounts.get(accountName);
            
        /* Deposit the money */
        account.deposit(amount);
        
        /* Check if it was a success */
        if(account.getHoldings() == oldAccount.getHoldings() + amount) {
            /* Update bank list */
            this.bankAccounts.put(accountName, account);
            return new TransactionResult(amount, account.getHoldings(), Type.SUCCESS, "'" + "' was successfully deposited to the bank account '" + accountName + "'.");
        }
        return new TransactionResult(0.0D, 0.0D, Type.ERROR, "An error occured while depositing '" + "' to the bank account '" + accountName + "'.");
    }

    /**
     * Check if a player is the owner of a bank account
     * @param accountName   Name of the bank account
     * @param playerName    Name of the player
     * @return              EconomyResponse Object
     */
    public TransactionResult isBankOwner(String accountName, String playerName) {
        /* Check if there's a bank by that name */
        if(!this.bankAccounts.containsKey(accountName))
            return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "There is no bank account by the name '" + accountName + "'.");
        
        /* Check if the player is the account owner */ 
        if(this.bankAccounts.get(accountName).getOwner() == playerName)
            return new TransactionResult(0.0D, 0.0D, Type.SUCCESS, "'" + playerName + "' is the owner of the bank account '" + accountName + "'.");

        /* Different owner */
        return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "The bank account '" + accountName + "' has a different owner.");
    }
    
    /**
     * Check if the player is a member of the bank account
     * @param accountName   Name of the bank account
     * @param playerName    Name of the player
     * @return              TransactionResult Object
     */
    public TransactionResult isBankMember(String accountName, String playerName) {
        /* Check if there's a bank by that name */
        if(!this.bankAccounts.containsKey(accountName))
            return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "There is no bank account by the name '" + accountName + "'.");

        /* Check if the player is a member of the account */ 
        if(this.bankAccounts.get(accountName).getMembers().contains(playerName))
            return new TransactionResult(0.0D, 0.0D, Type.SUCCESS, "'" + playerName + "' have access to the bank account '" + accountName + "'.");
        
        /* Not a member */
        return new TransactionResult(0.0D, 0.0D, Type.FAILURE, "'" + playerName + "' have no access to the bank account '" + accountName + "'.");
    }

    /**
     * Gets the list of bank accounts
     * @return the List of Bank accounts
     */
    public List<String> getBanks() {
        List<String> out = new ArrayList<String>();
        for(String accountName : this.bankAccounts.keySet()) {
            out.add(accountName);
        }
        return out;
    }
}
