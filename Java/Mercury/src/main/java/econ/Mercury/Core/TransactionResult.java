package econ.Mercury.Core;

public class TransactionResult {

    public static enum Type {
        /**
         * Used if process ended without any errors.
         */
        SUCCESS(1),
        /**
         * Used if process ended with failure.
         */
        FAILURE(2),
        /**
         * Used if process ended with an error.
         */
        ERROR(3);
        
        private int id;

        Type(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }
    }

    public final double amount;
    public final double balance;
    public final Type type;
    public final String message;

    /**
     * Constructor for TransactionResult
     * @param amount    Amount modified during operation
     * @param balance   New balance of account
     * @param type      Success or failure type of the operation
     * @param message   Message if necessary (commonly null)
     */
    public TransactionResult(Double amount, Double balance, Type type, String message) {
        this.amount = amount;
        this.balance = balance;
        this.type = type;
        this.message = message;
    }

    /**
     * Checks if an operation was successful
     * @return Value
     */
    public boolean transactionSuccess() {
        switch (type) {
        case SUCCESS:
            return true;
        default:
            return false;
        }
    }
}