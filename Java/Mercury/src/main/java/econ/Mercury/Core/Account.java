package econ.Mercury.Core;

public abstract class Account {

    protected String owner;
    protected Double holdings = 0.0D;
    
    public Account(String playerName) {
        this.owner = playerName;
    }
    
    public Double getHoldings() {
        return this.holdings;
    }
    
    public Boolean has(Double amount) {
        return this.holdings >= amount;
    }
    
    public boolean withdraw(Double amount) {
        if(this.has(amount)) {
            this.holdings -= amount;
            return true;
        } else
            return false;
    }
    
    public void deposit(Double amount) {
        this.holdings += amount;
    }

    public String getOwner() {
        return this.owner;
    }
}
