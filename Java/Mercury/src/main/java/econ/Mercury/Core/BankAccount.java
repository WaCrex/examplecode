package econ.Mercury.Core;

import java.util.ArrayList;
import java.util.List;

public class BankAccount extends Account {

    protected List<String> members = new ArrayList<String>();
    
    public BankAccount(String playerName) {
        super(playerName);
    }
    
    public List<String> getMembers() {
        return this.members;
    }
    
    public void addMember(String playerName) {
        this.members.add(playerName);
    }
    
    public void removeMember(String playerName) {
        this.members.remove(playerName);
    }
}
