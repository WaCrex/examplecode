package econ.Mercury.Core;

public class PlayerAccount extends Account {

    protected String world;

    public PlayerAccount(String playerName, String worldName) {
        super(playerName);
        this.world = worldName;
    }

    public String getWorld() {
        return this.world;
    }
}
