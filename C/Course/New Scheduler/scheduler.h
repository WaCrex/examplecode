#ifndef _SCHEDULER_H_
#define _SCHEDULER_H_

#ifdef __cplusplus
extern "C" {
#endif

/********************************* Includes **********************************/
#include <stdio.h>
#include <stdint.h>

/**************************** Constants and Types ****************************/
#define SCHEDULER_RATE              10u
#define SCHEDULER_TIME_DATATYPE     uint32_t

typedef void (*TaskRunner_t) (void);
typedef SCHEDULER_TIME_DATATYPE uTime_t; 

typedef struct {
    /**
     *  0   0    000000
     * <E> <R> <DEADLINE> 
     * 
     * == FLAGS ===
     * E         - Task is Enabled/Disabled (1/0)
     * R         - The task should be run
     *             after next clock reset.
     * 
     * DEADLINE  - The amount of time in ms that
     *             has been allocated to the task.
     */
    uint8_t flags;

    /**
     * The function that should be used to initialize the task.
     */
    TaskRunner_t init;
    
    /**
     * The function that the task should run.
     */
    TaskRunner_t runner;

    /**
     * This variable is used for two things:
     * 1. Counting down time until a task should be run.
     * 2. Counting how long the task has been READY for.
     */
    uTime_t start_time;

    /**
     * How often the task should be run.
     */
    uTime_t interval;
} Task_t;

/************************* Exported Functions ************************/
/**
 * @brief Initialize the scheduler with a static task queue.
 * 
 * The scheduler will run any existing init pointers and calculate
 * the start time before finaly storing a local copy of the task queue.
 *
 * @param count The amount of tasks in the array
 * @param taskqueue An array of tasks 
 */
void Scheduler_init(size_t count, Task_t taskqueue[]);

/**
 * @brief Use this function to set a task that should be run while
 * the scheduler is idle.
 * 
 * @param idleTask The runner for the idle task
 */
void Scheduler_setIdleTask(TaskRunner_t idleTask);

/**
 * @brief This function is used by the scheduler for counting the time passed.
 */
void Scheduler_update(void);

/**
 * @brief Run any tasks that are ready to be run.
 */
void Scheduler_run(void);


#ifdef __cplusplus
}
#endif

#endif//_SCHEDULER_H_