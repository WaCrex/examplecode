/***************************** Includes ******************************/
#include "scheduler.h"
#include <Arduino.h>
//#include <canbus.h>
//#include <constants.h>
#include <stdbool.h>
#include <string.h>

//#include "Sensors/TemperatureHumidity.h"
//#include "Sensors/SoilMoisture.h"
//#include "Sensors/Fertilizer.h"
//#include "Sensors/Water.h"
//#include "Sensors/Light.h"


/***************************** Variables *****************************/
IntervalTimer SCHEDULER_TIMER;
int LED_STATE = LOW;
static uint32_t lastBlink = millis();
static uint32_t lastPrint = lastBlink;
bool DEBUG = true;
static char status[][5] = {
  {'?', '?', '?', '?', '\0'},   // UNINITIALIZED
  {' ', 'O', 'K', ' ', '\0'},   // OK
  {'e', 'R', 'N', 'G', '\0'},   // RANGE_ERROR
  {'e', 'S', 'R', 'T', '\0'},   // SHORT_ERROR
  {'e', 'R', 'E', 'D', '\0'}    // READ_ERROR
};


/************************** Local Functions **************************/
void backSpace(int length) {
  char bs=8;
  for (int i=0;i<length;i++) {
    Serial.print(bs);
  }
}

/*void SerialMonitor() {
  // Get the current time
  uint32_t now = millis();

  // Check if it's time to change the state of the LED
  if ((now - lastBlink) >= 500) {
    // Toggle LED State
    LED_STATE = !LED_STATE;

    // Update LED State
    digitalWrite(LED_BUILTIN, LED_STATE);

    // Remember the time
    lastBlink = now;
  }

  if (DEBUG && ((now - lastPrint) >= 100)) {
    // Clear the console
    backSpace(150);

    // Print current sensor readings
    Serial.printf("LED:%s, T[%4s]:%.01lf°C, H[%4s]:%3d%%, M[%4s]:%3d%%, L[%4s]:%3d%%, W[%4s]:%3d%%, F[%4s]:%3d%%",
      (LED_STATE ? "ON " : "OFF"),

      status[getTemperatureStatus()], getTemperature(),
      status[getTemperatureStatus()], getHumidityLevel(),
      status[getMoistureStatus()], getMoistureLevel(),
      status[getLightIntensityStatus()], getLightIntensity(),
      status[getWaterLevelStatus()], getCurrentWaterLevel(),
      status[getFertilizeLevelStatus()], getCurrentFertilizeLevel()
    );

    // Remember the time
    lastPrint = now;
  }
}*/


/************************* Exported Functions ************************/
void setup() {
  // Start USB Monitor
  Serial.begin(9600);

  // Wait for the USB Monitor 
  delay(2000);

  // Tell teensy that we wanna write to the LED pin
  pinMode(LED_BUILTIN, OUTPUT);

  // Create a new static task queue
  Task_t taskqueue[] = {
    //{  30, initCAN,                   runCAN,                   0,   TIME_QUANTUM },
    //{  30, TemperatureHumidity_init,  TemperatureHumidity_run,  40,  2000         },
    //{  30, SoilMoisture_init,         SoilMoisture_run,         80,  2000         },
    //{  30, Fertilizer_init,           Fertilizer_run,           120, 2000         },
    //{  30, Water_init,                Water_run,                160, 2000         },
    //{  30, Light_init,                Light_run,                200, 2000         }
  };

  // Initialize the Scheduler with the newly created static task queue
  Scheduler_init(sizeof(taskqueue)/sizeof(Task_t), taskqueue);

  // Update the Scheduler every 100 microseconds
  SCHEDULER_TIMER.begin(Scheduler_update, SCHEDULER_RATE);

  // Set SerialMonitor as the idle task
  //Scheduler_setIdleTask(SerialMonitor);
}

void loop() {
  // Start the scheduler
  Scheduler_run();
}