#ifdef __cplusplus
extern "C" {
#endif

/********************************* Includes **********************************/
#include "scheduler.h"
#include <stdlib.h>
#include <string.h>


/**************************** Constants and Types ****************************/
#define __TIMER_MULTIPLIER      1000u

#define __TASK_COUNT_BITMASK    0x3F
#define __STATE1_SHOULDRESET    0x40
#define __STATE2_RESETING       0x80
#define __STATE3_WAITING        0xC0

#define __SET_TASK_COUNT(count) _flags |= (count & __TASK_COUNT_BITMASK)
#define __IS_STATE(state)       ((_flags & 0xC0) == state)
#define __SET_STATE(state)      _flags = (_flags & __TASK_COUNT_BITMASK) | state
#define __SET_STATE_NORMAL      ((_flags &= __TASK_COUNT_BITMASK))

#define __TASK_ENABLE(task)         task->flags |= 0x80
#define __TASK_DISABLE(task)        task->flags &= 0x7F
#define __TASK_IS_READY(task)       ((task->flags & 0xC0) == 0x80)      
#define __TASK_SET_RESET(task)      task->flags |= 0x40
#define __TASK_CLEAR_RESET(task)    task->flags &= 0xBF


/************************* Local Function Prototypes *************************/
int _run(TaskRunner_t runner, uTime_t deadline);
void _dummyTask(void);


/********************************* Variables *********************************/
//   00       000000
// <STATE> <TASK_COUNT>
//
// == STATES ===
// 00 - NONE
// 01 - Should reset task flags
// 10 - Reseting task flags
// 11 - Waiting for timer reset
static uint8_t
    _flags      = 0u;
static Task_t
   *_taskQueue  = NULL;
static TaskRunner_t
    _idleTask   = _dummyTask;
static const uTime_t
    UTIME_MAX   = -1u;
static uTime_t
    _intCount   = 0u,
    _sysTimer   = 0u,
    _wakeupTime = 1u;


/******************************* Local Functions *****************************/
int _run(TaskRunner_t runner, uTime_t deadline) {
    // Remember the current interrupt counter value
    uTime_t startCount = _intCount;
    
    // Run the task and cancel if the task didn't finish before the deadline
    while (((_intCount - startCount) * SCHEDULER_RATE) < deadline) {
        // Run the task's runner
        runner();

        // The task was finished in time
        return 0;
    }

    // The task wasn't finished in time
    return 1;
}

int _timerAdd(uTime_t *timer, uTime_t increment) {
    // Check if the timer should be reset
    if ((UTIME_MAX - *timer) > increment) {
        // Calculate the new timer value
        *timer = increment - (UTIME_MAX - *timer);

        // Tell the scheduler that the timer was reset
        return 1;
    } else {
        // Increase the timer
        *timer += increment;

        // Tell the scheduler that the timer wasn't reset
        return 0;
    }
}

void _timerAdvance(void) {
    // Check if there's a need to advance the timer
    if (_intCount > 0) {
        // Remember the current interrupt counter value
        uTime_t time = _intCount;

        // Safely reset the interrupt counter
        _intCount -= time;

        // Advance the system timer and return true if the timer was reset
        if (_timerAdd(&_sysTimer, time * SCHEDULER_RATE)) {
            // Set reset flag
            __SET_STATE(__STATE1_SHOULDRESET);
        }
    }
}

void _dummyTask(void) {}


/***************************** Exported Functions ****************************/
void Scheduler_init(size_t count, Task_t taskqueue[]) {
    for (Task_t *task = taskqueue; task <= (taskqueue + (count - 1)); task++) {
        // Apply bitmask on deadline
        task->flags &= 0x3F;

        // Check if the Task has an init function
        if (task->init != NULL) {
            // If so run it
            task->init();
        }

        // Calculate task start time
        task->start_time *= __TIMER_MULTIPLIER;

        // Calculate the interval time
        task->interval *= __TIMER_MULTIPLIER;

        // Check if this task should be run first
        //if (task->start_time < _wakeupTime) {
            // If so remember what time the scheduler should wake up at
        //    _wakeupTime = task->start_time;
        //}

        // Finaly enable the task
        __TASK_ENABLE(task);
    }

    // Allocate some memory for the task queue
    _taskQueue = (Task_t*) malloc(count * sizeof(Task_t));

    // Store the task queue
    memcpy(_taskQueue, taskqueue, count * sizeof(Task_t));

    // Store task queue size
    __SET_TASK_COUNT(count);
}

void Scheduler_setIdleTask(TaskRunner_t idleTask) {
    _idleTask = idleTask;
}

void Scheduler_update(void) {
    // Count the amount of interrupt calls
    _intCount++;
}

void Scheduler_run(void) {
    // Enter the main loop
    while (1) {
        // Advance the system timer
        _timerAdvance();

        // Check if there's work to do or the timer's been reset
        if ((_sysTimer >= _wakeupTime) || __IS_STATE(__STATE1_SHOULDRESET)) {
            // Check if the system timer has been reset
            if (__IS_STATE(__STATE1_SHOULDRESET)) {
                // Start reseting the task flags
                __SET_STATE(__STATE2_RESETING);
            }

            _wakeupTime = (_sysTimer == UTIME_MAX) ? 0 : _sysTimer + 1;

            // Iterrate through the tasks in the task queue
            for (Task_t *task = _taskQueue; task <= (_taskQueue + ((_flags & __TASK_COUNT_BITMASK) - 1)); task++) {
                // Check if the timer reset flag should be reset
                if (__IS_STATE(__STATE2_RESETING)) {
                    // If so reset it
                    __TASK_CLEAR_RESET(task);
                }

                // Check if it's time for the task to be run
                if (__TASK_IS_READY(task) && task->start_time <= _sysTimer) {
                    // Calculate the deadline and try to run the task's runner before the deadline
                    if (_run(task->runner, (task->flags & 0x3F) * __TIMER_MULTIPLIER)) {
                        // The task failed to finish within the allocated timeframe!
                        //printf("Failed to finish before the %d ms deadline!", (task->flags & 0x3F));
                    }

                    // Calculate next start time and return true if it's after next system timer reset
                    if (_timerAdd(&task->start_time, task->interval)) {
                        // Set reset flag
                        __TASK_SET_RESET(task);
                    }

                    // Check if this task should be run first within this clock
                    //if (!(task->flags & 0x40) && task->start_time < _wakeupTime) {
                        // If so remember what time the scheduler should wake up at
                        //_wakeupTime = task->start_time;
                    //}
                }

                // Advance the system timer
                _timerAdvance();
            }

            // Return to normal state
            __SET_STATE_NORMAL;
        } else {
            // Otherwize run the idle task instead
            _idleTask();
        }
    }
}


#ifdef __cplusplus
}
#endif