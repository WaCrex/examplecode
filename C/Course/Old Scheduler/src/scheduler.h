#ifndef _SCHEDULER_H
#define _SCHEDULER_H

/***************************** Settings ******************************/
#define TIME_DATA_TYPE uint16_t


/***************************** Includes ******************************/
#include <stdint.h>


/************************ Constants and Types ************************/
typedef enum {
    WAITING,
    READY,
    RUNNING,
    BLOCKING
} TaskState_t;

typedef struct Task_s {
    /**
     *   000000     00
     * <PRIORITY> <STATE> 
     *
     * PRIORITY  - The task priority level.
     * STATE     - The current runstate of the task.
     */
    uint8_t flags;

    /**
     * The name of the task.
     */
    char name[20];

    /**
     * The function that the task should run.
     */
    void (*runner)(void);

    /**
     * This variable is used for two things:
     * 1. Counting down time until a task should be run.
     * 2. Counting how long the task has been READY for.
     */
    TIME_DATA_TYPE runtime;

    /**
     * How often the task should be run.
     */
    TIME_DATA_TYPE interval;

    /**
     * The next entry in the Task Queue.
     */
    struct Task_s *next;
} Task_t;


/************************* Exported Functions ************************/
/**
 * @brief Create a new Task and add it to the Task Queue.
 * 
 * @param priority  The Task's priority level.
 * @param name      The name of the task.
 * @param runner    The function that the task should run.
 * @param offset    When the Task should run 
 * @param interval  How often the task should be run. If set to 0 the Task will only run once.
 */
void Scheduler_addTask(uint8_t priority, char name[20], void (*runner)(void), TIME_DATA_TYPE offset, TIME_DATA_TYPE interval);

/**
 * @brief This function is used by the scheduler for counting the time passed.
 */
void Scheduler_msTimerInterrupt(void);

/**
 * @brief Run any tasks that are ready to be run.
 */
void Scheduler_call(void);


/*********************** Unit Testing Functions **********************/
#ifdef TEST
Task_t *UnitTest_Scheduler_getTaskQueue();

#endif

#endif // _SCHEDULER_H
