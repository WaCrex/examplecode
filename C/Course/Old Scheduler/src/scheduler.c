/***************************** Includes ******************************/
#include "scheduler.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



static Task_t* _taskQueue;

uint8_t _Scheduler_timeCount = 0;

/************************** Local Functions **************************/
uint8_t _TIME_MS_COUNT(int reset) {
    if(reset) {
        return _Scheduler_timeCount = 0;
    } else {
        return  _Scheduler_timeCount;
    }
}
void _TASK_SET_STATE(Task_t* task, TaskState_t state) {
    task->flags = (task->flags & 0xFC) | state;
}


/************************* Exported Functions ************************/
void Scheduler_addTask(
    uint8_t priority,
    char name[20],
    void (*runner)(void),
    TIME_DATA_TYPE offset,
    TIME_DATA_TYPE interval
) {
    // Reset the counter, and count the time it takes to add the task
    _TIME_MS_COUNT(1);

    // Create a new task and allocate some memory for it
    Task_t* task = (Task_t*) malloc(sizeof(Task_t));

    // Set task priority and temporary enter BLOCKING state
    task->flags = ((priority & 0x3F) << 2) | 0x03;

    // Copy over the task name
    strcpy(task->name, name);

    // Make a pointer to the task's runner
    task->runner = runner;

    // Store how often the task should be run
    task->interval = interval;

    // Check if the Task Queue is empty
    if(_taskQueue == NULL) {
        // If so add this as the first task
        _taskQueue = task;
    } else {
        // Iterate through the task queue nodes
        for (
            Task_t *node = _taskQueue, *prev; node != NULL; prev = node, node = node->next
        ) {
            // Check if the new task has a higher priority than the current node
            if (task->flags > (node->flags & 0xFC)) {
                // Set the current node as the next node after the new task
                task->next = node;

                // Check if it's the beginning of the list
                if (prev == NULL) {
                    // If so add this as the first task
                    _taskQueue = task;
                } else {
                    // If not, set the previous task's next task as the new task
                    prev->next = task;
                }

                // As we are done we can now break the loop
                break;
            } else if (node->next == NULL) {
                // Add the new task if we have reached the end of the node list
                node->next = task;

                // As we are done we can now break the loop
                break;
            }
        }
    }

    // Get the time elapsed since the function was called
    uint8_t elapsed = _TIME_MS_COUNT(0);

    // Initialize the counter with a runtime that is as accurate as possible
    task->runtime = (offset >= elapsed) ? offset - elapsed : 0;

    // Enter WAITING state
    task->flags &= 0xFC;
}

void Scheduler_msTimerInterrupt(void) {
    // Count the time
    _Scheduler_timeCount++;
}



/*********************** Unit Testing Functions **********************/
#ifdef TEST
Task_t* UnitTest_Scheduler_getTaskQueue() {
    return _taskQueue;
}
void UnitTest_Scheduler_timeTick(int ticks) {
    _Scheduler_timeCount += ticks;
}
#endif
