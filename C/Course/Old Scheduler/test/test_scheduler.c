#include "unity.h"
#include "scheduler.h"

static Task_t* _taskQueue = NULL;

void setUp(void)
{
}

void tearDown(void)
{
}

/*********************** Unit Testing Functions **********************/
void ISR_testRunner() {

}

/*************************** Unit Testings ***************************/
void test_Scheduler_addTask(void)
{
    char MSG[255];

    TEST_ASSERT_NULL(_taskQueue);

    Scheduler_addTask(30, "Example Task 01", ISR_testRunner, 10, 1000);

    _taskQueue = UnitTest_Scheduler_getTaskQueue();

    TEST_ASSERT_NOT_NULL(_taskQueue);

    // Check if the Task's priority level has been properly set.
    sprintf(MSG, "Expected Priority level 30 for the new task but got %d!", (_taskQueue->flags >> 2));
    TEST_ASSERT_EQUAL_UINT8_MESSAGE(30, (_taskQueue->flags >> 2), MSG);

    // Check if the Task's run state has been properly set.
    sprintf(MSG, "Expected State 0x0 for the new task but got 0x%01X!", (_taskQueue->flags & 0x3));
    TEST_ASSERT_EQUAL_UINT8_MESSAGE(0x0, (_taskQueue->flags & 0x3), MSG);

    // Check if the Task's name has been properly set.
    sprintf(MSG, "Expected the name 'Example Task 01' for the new task but got '%s'!", _taskQueue->name);
    TEST_ASSERT_EQUAL_STRING_MESSAGE("Example Task 01", _taskQueue->name, MSG);

    // Check if the Task's runner has been properly set.
    sprintf(MSG, "Expected runner to point to the address '%X' but it pointed to '%X' instead!", ISR_testRunner, _taskQueue->runner);
    TEST_ASSERT_EQUAL_PTR_MESSAGE(ISR_testRunner, _taskQueue->runner, MSG);

}
