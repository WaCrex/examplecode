/**
 * \file    display.h
 * \author  Patrick Lineruth
 * \note    Note I've redacted the date and some file and/or variable names for
 *          personal reasons.
 */

#ifndef DISPLAY_H_INCLUDED
#define DISPLAY_H_INCLUDED

/* ========== Public Includes =============================================== */
#include "redacted_conf.h"
#include "color.h"
#include <stdint.h>
#include <stdbool.h>

/* ========== Public Definitions ============================================ */
#define __DISPLAY_WIDTH     (__DISPLAY_PANEL_WIDTH * __DISPLAY_PANEL_COUNT_W)
#define __DISPLAY_HEIGHT    (__DISPLAY_PANEL_HEIGHT * __DISPLAY_PANEL_COUNT_H)

/* ========== Public Functions ============================================== */
/**
 * @brief Initialize the framebuffer
 */
void Display_init(void);

/**
 * @brief Colors a pixel on the display.
 * @param x		The distance to the pixel from the left
 * @param y		The distance to the pixel from the top
 * @param color	The color to be drawn
 */
void Display_setPixel(uint16_t x, uint16_t y, Color_t color);

/**
 * @brief Tell the display that the frame is ready to be drawn onto the matrix.
 */
void Display_draw(void);

/**
 * @brief Adjust the display gamma.
 * @param gamma     A value between 0-50
 */
void Display_setGamma(uint8_t gamma);

/**
 * @brief Uses timer compare to adjust the display brightness.
 * @param intensity The brightness level 0-100.
 */
void Display_setIntensity(uint8_t intensity);

#endif//DISPLAY_H_INCLUDED
