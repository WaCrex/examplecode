/**
 * \file    flash.h
 * \brief   Handles flash reading and writing.
 * \author  Patrick Lineruth
 * \note    Note I've redacted the date and some file and/or variable names for 
 *          personal reasons.
 */

/**
 * \addtogroup Config
 * @{
 */
#ifndef __FLASH_H__
#define __FLASH_H__

/* ========== Public Includes =============================================== */
#include <crc.h>
#include <stdbool.h>
#include <sys/_stdint.h>

#ifdef __cplusplus
extern "C"
{
#endif

/* ========== Public Definitions ============================================ */
#define CRC_TABLE_ADDR  0x00000000
#define USE_SPI_FLASH   1

/* ========== Public Functions ============================================== */
/**
 * \brief   Initialise the flash and read the CRC Table.
 *
 * \note    Nothing will happen if called more than once.
 */
void Flash_init(void);

/**
 * \brief   Reads data from flash.
 *
 * \param [in]      addr    The address to read from
 * \param [out]     rxBuff  Where to store the data
 * \param [in]      len     The amount of data to read
 */
void Flash_readWithoutCRC(uint32_t addr, uint8_t *rxBuff, uint32_t len);

/**
 * \brief   Reads data from flash and compares the CRC with what it just read.
 *
 * \param [in]      addr    The address to read from
 * \param [out]     rxBuff  Where to store the data if the CRC matches
 * \param [in]      len     The amount of data to read
 * \param [in]      crc     The CRC that it should compare it with
 * \return                  True if the CRC matches, False if it doesn't
 */
bool Flash_readWithCRC(uint32_t addr, uint8_t *rxBuff, uint32_t len, CRC_t crc);

/**
 * \brief   Reads data from flash using FLASH_ReadWithCR() where the CRC is\n
 *          read from the CRC Table.
 *
 * \param [in]      addr    The address to write to
 * \param [out]     txBuff  The data to be written
 * \param [in]      len     The amount of data to write
 * \param [in]      index   The CRC Table index that should be used
 * \return                  True if the write succeeded, False it failed
 */
bool Flash_readWithCRCIndex(uint32_t addr, uint8_t *rxBuff, uint32_t len,
        uint16_t index);

/**
 * \brief   Writes data to flash, skips CRC check.
 *
 * \param [in]      addr    The address to write to
 * \param [in]      txBuff  The data to be written
 * \param [in]      len     The amount of data to write
 */
void Flash_writeWithoutCRC(uint32_t addr, uint8_t *txBuff, uint32_t len);

/**
 * \brief   Writes data to flash, afterward it reads the data from the flash\n
 *          and compares the CRC values.
 *
 * \param [in]      addr    The address to write to
 * \param [in]      txBuff  The data to be written
 * \param [in]      len     The amount of data to write
 * \param [out]     crc     Where to store the CRC once done
 * \return                  True if the write succeeded, False it failed
 */
bool Flash_writeWithCRC(uint32_t addr, uint8_t *txBuff, uint32_t len,
        CRC_t *crc);

/**
 * \brief   Writes data to flash using FLASH_WriteWithCR(), afterward it\n
 *          stores the CRC in the CRC Table.
 *
 * \param [in]      addr    The address to write to
 * \param [in]      txBuff  The data to be written
 * \param [in]      len     The amount of data to write
 * \param [in]      index   The CRC Table index that should be used
 * \return                  True if the write succeeded, False it failed
 */
bool Flash_writeWithCRCIndex(uint32_t addr, uint8_t *txBuff, uint32_t len,
        uint16_t index);

/**
 * @brief   Checks if the flash is busy.
 *
 * @return                  True if busy, False if not
 */
bool Flash_busy(void);

#ifdef __cplusplus
}
#endif

#endif//__FLASH_H__

/**
 * @}
 */

