/**
 * \file    display.c
 * \author  Patrick Lineruth
 * \author  Frans-Willem
 * \note    This file is a heavily modified version of the files framebuffer.c &
 *          colorcorr.c from the repo https://github.com/Frans-Willem/LEDMatrixHUB75/
 * 
 * \note    Note I've redacted the date and some file and/or variable names for
 *          personal reasons.
 */

/* ========== Private Includes ============================================== */
#include "display.h"
#include "redacted.h"
#include "config.h"
#include <stm32f4xx.h>
#include <math.h>
#include <string.h>

/* ========== Private Definitions =========================================== */
#define __DISPLAY_SCANROWS \
        (__DISPLAY_PANEL_HEIGHT / __DISPLAY_PANEL_BUSSES)
#define __DISPLAY_SHIFTLEN          (__DISPLAY_WIDTH * 2)
#define __DISPLAY_MAX_BITDEPTH      11
#define __DISPLAY_COLOR_CHANNELS    3
#define __DISPLAY_ROW_LENGHT \
        (__DISPLAY_SHIFTLEN * __DISPLAY_MAX_BITDEPTH)
#define __DISPLAY_BUFFER_SIZE \
        (__DISPLAY_ROW_LENGHT * __DISPLAY_SCANROWS)
#if (__DISPLAY_DOUBLE_BUFF == 1)
#define _BUFFER_SIZE                (__DISPLAY_BUFFER_SIZE * 2)
#else
#define _BUFFER_SIZE                __DISPLAY_BUFFER_SIZE
#endif

/* ========== Private Variables ============================================= */
uint16_t _Display_gammaTable[1 + __COLOR_GAMMA_MAX][256];
uint8_t _Display_gamma;
uint8_t _Display_scanrow = __DISPLAY_SCANROWS - 1;
uint8_t _Display_framebuffer[_BUFFER_SIZE] = {0};
bool swap_required = 0;

/* ========== Private Functions ============================================= */
void _initGAMMA(void);
void _initGPIO(void);
void _initTIM(void);
void _initDMA(void);
void _initNVIC(void);
void _Display_next(void);

/* ========== Public Functions ============================================== */
void Display_init()
{
    // Clock the buffer data ahead of time
    for (uint32_t i = 0; i < _BUFFER_SIZE; i++) {
        _Display_framebuffer[i] = (i & 1) ? GPIO_Pin_6 : 0;
    }

    // Initialise gamma table
    _initGAMMA();

    // Setup the display
    _initGPIO();
    _initTIM();
    _initDMA();
    _initNVIC();

    // Set the display brightness to what's stored in config
    Display_setIntensity(redacted.config.intensity);

    __DISPLAY_OE_TIM->PSC = 0;
    _Display_next();
}

void Display_draw()
{
    swap_required = true;
}

void Display_setGamma(uint8_t gamma)
{
    redacted.config.gamma = gamma;
    Config_setFlag(0, 0);
}

void Display_setIntensity(uint8_t intensity) {
    TIM_SetCompare1(__DISPLAY_OE_TIM,
            (intensity * __DISPLAY_MIN_DISPLAY_TIME) / 255);
    Config_setFlag(0, 0);
}

void Display_setPixel(uint16_t x, uint16_t y, Color_t color) {
    // Make sure we aren't trying to draw outside the display
    if (y >= __DISPLAY_HEIGHT || x >= __DISPLAY_WIDTH)
        return;

    uint16_t rgb[] = {
        _Display_gammaTable[_Display_gamma][color.red],
        _Display_gammaTable[_Display_gamma][color.green],
        _Display_gammaTable[_Display_gamma][color.blue]
    };

    uint8_t scanrow    = y % __DISPLAY_SCANROWS;
    uint8_t bus        = (y / __DISPLAY_SCANROWS) % __DISPLAY_PANEL_BUSSES;
#if (__DISPLAY_PANEL_COUNT_H > 1)
    uint16_t segment	= y / __DISPLAY_PANEL_HEIGHT;

    // The address we should write to
    uint32_t offset = (scanrow * __DISPLAY_ROW_LENGHT) +
            ((__DISPLAY_PANEL_COUNT_H - 1 - segment) * __DISPLAY_PANEL_COUNT_W *
            __DISPLAY_PANEL_WIDTH * 2) + (x * 2);
#else
    // The address we should write to
    uint32_t offset = (scanrow * __DISPLAY_ROW_LENGHT) + (x * 2);
#endif

#if (__DISPLAY_DOUBLE_BUFF == 1)
    uint8_t *ptr = &_Display_framebuffer[__DISPLAY_BUFFER_SIZE + offset];
#else
    uint8_t *ptr = &_Display_framebuffer[offset];
#endif

    for (uint16_t bit = (1 << (__DISPLAY_MAX_BITDEPTH - 1)); bit; bit>>=1)
    {
        for (uint8_t channel = 0; channel < __DISPLAY_COLOR_CHANNELS; channel++)
        {
            uint8_t pin = 0x1 << ((bus * __DISPLAY_COLOR_CHANNELS) + channel);
            if (rgb[channel] & bit)
            {
                ptr[0] |= pin;
                ptr[1] |= pin;
            }
            else
            {
                ptr[0] &= ~pin;
                ptr[1] &= ~pin;
            }
        }
        ptr = &ptr[__DISPLAY_SHIFTLEN];
    }
}

/* ========== Interrupt Handers ============================================= */
void __DISPLAY_OE_TIM_IRQH()
{
    //Disable timer
    __DISPLAY_OE_TIM->CR1 &= ~TIM_CR1_CEN;
    //Clear interrupt flags (maybe delay this?)
    __DISPLAY_OE_TIM->SR &= ~TIM_IT_CC2;
    if (!(__DISPLAY_DMA_STREAM->CR & DMA_SxCR_EN)) {
        _Display_next();
    }
}

void __DISPLAY_DMA_IRQH() {
    //Disable DMA (is this even needed?) (apparently not)
    //__DISPLAY_DMA_STREAM->CR &= ~DMA_SxCR_EN;
    //Clear interrupt flags (maybe delay this?)
    __DISPLAY_DMA->HIFCR = DMA_HIFCR_CTCIF5;
    //If the timer has already stopped running, queue up the next row.
    if (!(__DISPLAY_OE_TIM->CR1 & TIM_CR1_CEN))
        _Display_next();
    return;
}

/* ========== Private Functions ============================================= */
void _Display_next()
{
    // Strobe up
    __DISPLAY_CONTROL->BSRRL = __DISPLAY_CONTROL_STB_PIN;
    // Update curdata pointer
    __DISPLAY_DMA_STREAM->M0AR += __DISPLAY_SHIFTLEN * sizeof(uint8_t);
    // Select scanrow while keeping strobe up
    __DISPLAY_CONTROL->ODR = (_Display_scanrow << __DISPLAY_CONTROL_BITSHIFT) |
    		__DISPLAY_CONTROL_STB_PIN;
    // Load the timer prescaler
    __DISPLAY_OE_TIM->EGR = TIM_PSCReloadMode_Immediate;

    if (__DISPLAY_OE_TIM->PSC == 0)
    {
#if (__DISPLAY_DOUBLE_BUFF == 1)
        // Swap the framebuffer if needed
        if (swap_required) {
            memcpy(&_Display_framebuffer,
                    &_Display_framebuffer[__DISPLAY_BUFFER_SIZE],
                    __DISPLAY_BUFFER_SIZE);
            swap_required = false;
        }
#endif
        // Update gamma once the frame is done
        _Display_gamma  = redacted.config.gamma;

        __DISPLAY_OE_TIM->PSC = (1 << __DISPLAY_MAX_BITDEPTH) - 1;

        _Display_scanrow++;

        if (_Display_scanrow == __DISPLAY_SCANROWS) {
            _Display_scanrow = 0;
            __DISPLAY_DMA_STREAM->M0AR = (uint32_t) _Display_framebuffer;
        }
    }
    __DISPLAY_OE_TIM->PSC >>= 1;

    // Strobe down (deliberately quite far away from strobe up to give the
    // panels some time to respond)
    __DISPLAY_CONTROL->BSRRH = __DISPLAY_CONTROL_STB_PIN;
    //Actually display what is already in the buffer
    // Output is enabled over this loop, seeing as this will always take the
    // same amount of time.
    __DISPLAY_OE_TIM->CNT = 0; //Set counter to 0
    //Clear any flags
    __DISPLAY_DMA->LIFCR = 0b111101;
    //Enable DMA and Timer
    __DISPLAY_OE_TIM->CR1 |= TIM_CR1_CEN;
    __DISPLAY_DMA_STREAM->CR |= DMA_SxCR_EN;
}

float _Display_lum2duty(double lum)
{
    if (lum>0.07999591993063804)
        return (pow(((lum + 0.16) / 1.16), 3.0));
	else
        return (lum / 9.033);
}

void _Display_lumgamma(uint16_t *table, double gamma)
{
    for (uint16_t i = 0, oi; i < 256; i++)
    {
        double di = pow(_Display_lum2duty(((double) i) / 255.0), gamma);
        oi = (uint16_t) (di * (double) ((1 << __DISPLAY_MAX_BITDEPTH) - 1));
        if (oi > (1 << __DISPLAY_MAX_BITDEPTH)-1)
            oi = (1 << __DISPLAY_MAX_BITDEPTH)-1;
        table[i ] = oi;
    }
}

void _initGAMMA()
{
    for (uint16_t i = 0; i < 256; i++)
    {
        _Display_gammaTable[0][i] = (i*((1 << __DISPLAY_MAX_BITDEPTH)-1))/255;
    }

    for (uint16_t i = 0; i <= __COLOR_GAMMA_MAX; i++)
    {
        _Display_lumgamma(_Display_gammaTable[i + 1],
                __COLOR_GAMMA_MIN + (__COLOR_GAMMA_STEP * i));
    }
}

void _initGPIO()
{
    GPIO_InitTypeDef GPIO_InitStruct;

    // Enable peripheral clock
    __DISPLAY_OE_INIT(__DISPLAY_OE_CLK, ENABLE);
    __DISPLAY_DATA_INIT(__DISPLAY_DATA_CLK, ENABLE);
    __DISPLAY_CONTROL_INIT(__DISPLAY_CONTROL_CLK, ENABLE);

    // OE - Change pin mapping
    GPIO_PinAFConfig(__DISPLAY_OE, __DISPLAY_OE_SOURCE, __DISPLAY_OE_AF);

    // OE - Configure GPIO
    GPIO_StructInit(&GPIO_InitStruct);
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
    GPIO_InitStruct.GPIO_Speed = GPIO_High_Speed;
    GPIO_InitStruct.GPIO_Pin = __DISPLAY_OE_PIN;
    GPIO_Init(__DISPLAY_OE, &GPIO_InitStruct);

    // DATA - Configure GPIO
    GPIO_StructInit(&GPIO_InitStruct);
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
    GPIO_InitStruct.GPIO_Speed = GPIO_High_Speed;
    GPIO_Init(__DISPLAY_DATA, &GPIO_InitStruct);
    __DISPLAY_DATA->ODR=0;

    // CONTROL - Configure GPIO
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_Speed = GPIO_High_Speed;
	//For control, only row and control pins
	GPIO_InitStruct.GPIO_Pin = ((__DISPLAY_SCANROWS - 1) << __DISPLAY_CONTROL_SHIFT) | __DISPLAY_CONTROL_STB_PIN;
	GPIO_Init(__DISPLAY_CONTROL, &GPIO_InitStruct);
}

void _initTIM()
{
    TIM_TimeBaseInitTypeDef  TIM_TimeBaseStruct;
    TIM_OCInitTypeDef TIM_OCStruct;

    // Enable peripheral clock
    __DISPLAY_OE_TIM_INIT(__DISPLAY_OE_TIM_CLK, ENABLE);
    __DISPLAY_DMA_TIM_INIT(__DISPLAY_DMA_TIM_CLK, ENABLE);

    // Configure APB Clock
    __DISPLAY_OE_RCC_INIT(__DISPLAY_OE_RCC_CLK);

    // OE - Time base configuration
    TIM_TimeBaseStructInit(&TIM_TimeBaseStruct);
    TIM_TimeBaseStruct.TIM_Period = __DISPLAY_OE_TIM_PERIOD;
    TIM_TimeBaseStruct.TIM_Prescaler = 1;
    TIM_TimeBaseInit(__DISPLAY_OE_TIM, &TIM_TimeBaseStruct);

    // OE - Timer configuration
    TIM_OCStructInit(&TIM_OCStruct);
    TIM_OCStruct.TIM_OCMode = TIM_OCMode_PWM1;
    TIM_OCStruct.TIM_OCPolarity = TIM_OCPolarity_Low;
    TIM_OCStruct.TIM_OutputState = TIM_OutputState_Enable;
    TIM_OCStruct.TIM_Pulse = __DISPLAY_MIN_DISPLAY_TIME;
    TIM_OC1Init(__DISPLAY_OE_TIM, &TIM_OCStruct);
    TIM_OCStruct.TIM_Pulse = __DISPLAY_MIN_DISPLAY_TIME;
    TIM_OC2Init(__DISPLAY_OE_TIM, &TIM_OCStruct);

    // DMA - Time base configuration
    TIM_TimeBaseStructInit(&TIM_TimeBaseStruct);
    TIM_TimeBaseStruct.TIM_Period = 1;
    TIM_TimeBaseStruct.TIM_CounterMode = TIM_CounterMode_Down;
    TIM_TimeBaseInit(TIM1, &TIM_TimeBaseStruct);
    TIM_Cmd(__DISPLAY_DMA_TIM, ENABLE);

    // Enable timer interrupt
    TIM_ITConfig(__DISPLAY_OE_TIM, TIM_IT_CC2, ENABLE);
}

void _initDMA()
{
    DMA_InitTypeDef DMA_InitStruct;

    // Enable peripheral clock
    __DISPLAY_DMA_INIT(__DISPLAY_DMA_CLK, ENABLE);

    //Clear flags
    DMA_ClearFlag(__DISPLAY_DMA_STREAM, DMA_FLAG_FEIF5 | DMA_FLAG_DMEIF5 |
            DMA_FLAG_TEIF5 | DMA_FLAG_HTIF5 | DMA_FLAG_TCIF5);

    // Enable DMA stream
    DMA_Cmd(__DISPLAY_DMA_STREAM, DISABLE);

    // Deinitialize DMA stream registers
    DMA_DeInit(__DISPLAY_DMA_STREAM);

    DMA_StructInit(&DMA_InitStruct);
    DMA_InitStruct.DMA_Channel = __DISPLAY_DMA_CHANNEL;
    DMA_InitStruct.DMA_PeripheralBaseAddr = (uint32_t) &__DISPLAY_DATA->ODR;
    DMA_InitStruct.DMA_DIR = DMA_DIR_MemoryToPeripheral;
    DMA_InitStruct.DMA_BufferSize = __DISPLAY_SHIFTLEN;
    DMA_InitStruct.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStruct.DMA_MemoryBurst = DMA_MemoryBurst_INC16;
    DMA_InitStruct.DMA_Priority = DMA_Priority_VeryHigh;
    DMA_InitStruct.DMA_FIFOMode = DMA_FIFOMode_Enable;
    DMA_InitStruct.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
    DMA_Init(__DISPLAY_DMA_STREAM, &DMA_InitStruct);

    DMA_FlowControllerConfig(__DISPLAY_DMA_STREAM, DMA_FlowCtrl_Memory);

    // Enable DMA interrupt
    DMA_ITConfig(__DISPLAY_DMA_STREAM, DMA_IT_TC, ENABLE);

    // Enable timer DMA request
    TIM_DMACmd(__DISPLAY_DMA_TIM, TIM_DMA_Update, ENABLE);
}

void _initNVIC(void)
{
    NVIC_InitTypeDef NVIC_InitStruct;

    // OE - NVIC Configuration
    NVIC_InitStruct.NVIC_IRQChannel = __DISPLAY_OE_TIM_IRQ;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 1;
    NVIC_Init(&NVIC_InitStruct);

    // DMA - NVIC Configuration
    NVIC_InitStruct.NVIC_IRQChannel = __DISPLAY_DMA_IRQ;
    NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
    NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 1;
    NVIC_InitStruct.NVIC_IRQChannelSubPriority = 1;
    NVIC_Init(&NVIC_InitStruct);
}

