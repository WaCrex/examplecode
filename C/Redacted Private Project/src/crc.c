/**********************************************************************
 *
 * Filename:    crc.c
 * 
 * Description: Slow and fast implementations of the CRC standards.
 *
 * Notes:       The parameters for each supported CRC standard are
 *				defined in the header file crc.h.  The implementations
 *				here should stand up to further additions to that list.
 *
 * 
 * Copyright (c) 2000 by Michael Barr.  This software is placed into
 * the public domain and may be used for any purpose.  However, this
 * notice must not be changed or removed and no warranty is either
 * expressed or implied by its publication or distribution.
 **********************************************************************/

/**
 * \file    crc.c
 * \author  Michael Barr
 * \author  Patrick Lineruth
 * \note    Note I've redacted the date and some file and/or variable names for 
 *          personal reasons.
 */

/* ========== Private Includes ============================================== */
#include <crc.h>
#include <sys/_stdint.h>

/* ========== Private Function Prototypes =================================== */
static uint32_t _CRC_reflect(uint32_t data, uint8_t len);

/* ========== Private Definitions =========================================== */
/*
 * Derive parameters from the standard-specific parameters in crc.h.
 */
#define WIDTH    (8 * sizeof(CRC_t))
#define TOPBIT   (1 << (WIDTH - 1))

#if (REFLECT_DATA == TRUE)
#undef  REFLECT_DATA
#define REFLECT_DATA(X)         ((uint8_t) _CRC_reflect((X), 8))
#else
#undef  REFLECT_DATA
#define REFLECT_DATA(X)         (X)
#endif

#if (REFLECT_REMAINDER == TRUE)
#undef  REFLECT_REMAINDER
#define REFLECT_REMAINDER(X)    ((CRC_t) _CRC_reflect((X), WIDTH))
#else
#undef  REFLECT_REMAINDER
#define REFLECT_REMAINDER(X)    (X)
#endif

/* ========== Private Variables ============================================= */
CRC_t _CRC_table[256];

/* ========== Private Functions ============================================= */
void _CRC_modulo2(uint16_t *remainder)
{
    // Perform modulo-2 division, a bit at a time.
    for (uint8_t bit = 0x80; bit != 0x00; bit >>= 1)
    {
        // Try to divide the current data bit.
        *remainder <<= 1;
        if (*remainder & TOPBIT)
            *remainder ^= POLYNOMIAL;
    }
}

static uint32_t _CRC_reflect(uint32_t data, uint8_t len)
{
    uint32_t reflection = 0x00000000;

    // Reflect the data about the center bit.
    for (uint8_t bit = 0; bit < len; ++bit)
    {
        // If the LSB bit is set, set the reflection of it.
        if (data & 0x01)
            reflection |= (1 << ((len - 1) - bit));

        data >>= 1;
    }

    return (reflection);
}

/* ========== Public Functions ============================================== */
void CRC_init(void)
{
    CRC_t remainder;

    // Compute the remainder of each possible dividend.
    for (uint16_t dividend = 0; dividend < 256; ++dividend)
    {
        // Start with the dividend followed by zeros.
        remainder = dividend << (WIDTH - 8);

        // Perform modulo-2 division, a bit at a time.
        _CRC_modulo2(&remainder);

        // Store the result into the table.
        _CRC_table[dividend] = remainder;
    }
}

CRC_t CRC_slow(const uint8_t *data, uint16_t len)
{
    CRC_t remainder = INITIAL_REMAINDER;

    // Perform modulo-2 division, a byte at a time.
    for (uint16_t byte = 0; byte < len; ++byte)
    {
        // Bring the next byte into the remainder.
        remainder ^= (REFLECT_DATA(data[byte]) << (WIDTH - 8));

        // Perform modulo-2 division, a bit at a time.
        _CRC_modulo2(&remainder);
    }

    // The final remainder is the CRC result.
    return (REFLECT_REMAINDER(remainder) ^ FINAL_XOR_VALUE);
}

CRC_t CRC_fast(const uint8_t *data, uint16_t len)
{
    CRC_t remainder = INITIAL_REMAINDER;

    // Divide the message by the polynomial, a byte at a time.
    for (uint8_t byte = 0, _data; byte < len; ++byte)
    {
        _data = REFLECT_DATA(data[byte]) ^ (remainder >> (WIDTH - 8));
        remainder = _CRC_table[_data] ^ (remainder << 8);
    }

    // The final remainder is the CRC.
    return (REFLECT_REMAINDER(remainder) ^ FINAL_XOR_VALUE);
}
