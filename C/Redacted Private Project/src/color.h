/**
 * \file    color.h
 * \brief   Provides an easy to use interface for handling color codes.
 * \author  Patrick Lineruth
 * \note    Note I've redacted the date and some file and/or variable names for
 *          personal reasons.
 */

/** \addtogroup Display
 * @{
 */

#ifndef __COLOR_H__
#define __COLOR_H__

#ifdef __cplusplus
extern "C"
{
#endif

/* ========== Public Includes =============================================== */
#include <stdint.h>

/* ========== Public Definitions ============================================ */
// Legacy Colors
#define BLACK                   ((Color_t) {0x00, 0x00, 0x00})
#define RED                     ((Color_t) {0xAA, 0x00, 0x00})
#define GREEN                   ((Color_t) {0x00, 0xAA, 0x00})
#define ORANGE                  ((Color_t) {0xFF, 0x20, 0x00})

#define DEFAULT_COLORS          64
#define USER_COLORS             (256 - DEFAULT_COLORS)

/* ========== Public Type Definitions ======================================= */
typedef struct
{
    uint8_t red;
    uint8_t green;
    uint8_t blue;
} Color_t;

/* ========== Public Macros ================================================= */
/**
 * Creates a new Color_t using the supplied color values.
 */
#define Color(red, green, blue) ((Color_t) {red, green, blue})

/* ========== Public Functions ============================================== */
/**
 * \brief   Retrieves RGB Color Code using color index.
 *
 * \note    The colors are split into two sections:
 *          - Colors 0 to 63, are pre-defined default colors, these are\n
 *            non-changeable.
 *          - Colors 64 to 255, are user defined colors, these can be changed\n
 *            at any time and are stored on the flash.
 *
 * @param [in]      i       The index of the color
 * @return                  An 24-bit representation of the color
 */
Color_t Color_getColor(uint8_t i);

/**
 * \brief   Retrieves RGB Color code for legacy color using color index.
 *
 * \param [in]      i       The index of the legacy color
 * \return                  An 24-bit representation of the color
 */
Color_t Color_getLegacyColor(uint8_t i);

/**
 * \brief   Whenever or not the color has been defined by the user or not.
 * \param [in]      i       The index of the color
 * \return                  1 if true and 0 if false
 */
uint8_t Color_inUse(uint8_t i);

/**
 * \brief   Resets a user defined color to it's default value and stores it on\n
 *          the flash.
 *
 * \param [in]      i       The index of the color
 */
void Color_resetColor(uint8_t i);

/**
 * \brief   Define a user color and store it on the flash.
 *
 * \param [in]      i       The index of the color (must be between 64 and 255)
 * \param [in]      rgb     The color that should be stored
 */
void Color_setColor(uint8_t i, Color_t rgb);

#ifdef __cplusplus
}
#endif

#endif//__COLOR_H__

/**
 * @}
 */
