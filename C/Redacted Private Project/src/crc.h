/**********************************************************************
 *
 * Filename:    crc.h
 * 
 * Description: A header file describing the various CRC standards.
 *
 * Notes:       
 *
 * 
 * Copyright (c) 2000 by Michael Barr.  This software is placed into
 * the public domain and may be used for any purpose.  However, this
 * notice must not be changed or removed and no warranty is either
 * expressed or implied by its publication or distribution.
 **********************************************************************/

/**
 * \file    crc.h
 * \brief   Slow and fast implementations of the CRC standards.
 * \author  Michael Barr
 * \author  Patrick Lineruth
 *
 * \note    The parameters for each supported CRC standard are
 *          defined in the header file crc.h.  The implementations
 *          here should stand up to further additions to that list.
 * 
 * \note    Note I've redacted the date and some file and/or variable names
 *          for personal reasons.
 *
 * \b Original Source:
 * https://barrgroup.com/Embedded-Systems/How-To/CRC-Calculation-C-Code
 */

/** \addtogroup Security
  * @{
  */
#ifndef __CRC_H__
#define __CRC_H__

/* ========== Public Includes =============================================== */
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

/* ========== Public Definitions ============================================ */
/**
 * Select the CRC standard from the list that follows.
 */
#define CRC_CCITT

#if defined(CRC_CCITT)
#define CRC_NAME            "CRC-CCITT"
#define POLYNOMIAL          0x1021
#define INITIAL_REMAINDER   0xFFFF
#define FINAL_XOR_VALUE     0x0000
#define REFLECT_DATA        false
#define REFLECT_REMAINDER   false
#define CHECK_VALUE         0x29B1

#elif defined(CRC16)
#define CRC_NAME            "CRC-16"
#define POLYNOMIAL          0x8005
#define INITIAL_REMAINDER   0x0000
#define FINAL_XOR_VALUE     0x0000
#define REFLECT_DATA        true
#define REFLECT_REMAINDER   true
#define CHECK_VALUE         0xBB3D

#elif defined(CRC32)
#define CRC_NAME            "CRC-32"
#define POLYNOMIAL          0x04C11DB7
#define INITIAL_REMAINDER   0xFFFFFFFF
#define FINAL_XOR_VALUE     0xFFFFFFFF
#define REFLECT_DATA        true
#define REFLECT_REMAINDER   true
#define CHECK_VALUE         0xCBF43926

#else
#error "One of CRC_CCITT, CRC16, or CRC32 must be #define'd."
#endif


/* ========== Public Type Definitions ======================================= */
#if defined(CRC_CCITT) || defined(CRC16)
typedef uint16_t  CRC_t;
#elif defined(CRC32)
typedef uint32_r CRC_t;

#endif


/* ========== Public Functions ============================================== */
/**
 * \brief   Populate the partial CRC lookup table.
 *
 * \note    This function must be rerun any time the CRC standard
 *          is changed.  If desired, it can be run "offline" and
 *          the table results stored in an embedded system's ROM.
 */
void CRC_init(void);

/**
 * \brief   Compute the CRC of given data.
 *
 * \param [in]  data    The data to be computed
 * \param [in]  len     The length of the data
 * \return              The CRC of the data.
 */
CRC_t CRC_slow(const uint8_t *data, uint16_t len);

/**
 * \brief   Compute the CRC of given data. CRC_Init() must be called first.
 *
 * \param [in]  data    The data to be computed
 * \param [in]  len     The length of the data
 * \return              The CRC of the data.
 */
CRC_t CRC_fast(const uint8_t *data, uint16_t len);


#ifdef __cplusplus
}
#endif

#endif//__CRC_H__

/**
  * @}
  */
