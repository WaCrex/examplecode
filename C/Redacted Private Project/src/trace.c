/**
 * \file    trace.c
 * \author  Patrick Lineruth
 * \author  Unknown
 * \note    I got help with the assembler part from a couple of blog posts,
 *          don't remember the addresses, so I can only credit them as Unknown.
 * 
 * \note    Note I've redacted the date and some file and/or variable names for
 *          personal reasons.
 */

#ifdef DEBUG
#include "trace.h"
#include "timer.h"
#include <stdio.h>
#include <stdint.h>
#include <string.h>

void TTRACE(TTRACELevel_t level, const char *msg) {
    char buff[strlen(msg) + 22 + 255];
    const Timer_t timer = Timer_getTime();
    sprintf(buff, "%02d:%02d:%02d [%s]: %s", timer.hours, timer.minutes,
            timer.seconds, ((level == 0) ? "INFO" : (level == 1) ? "WARNING" :
            "ERROR"), msg);
    uint32_t m[] = { ((level == 2) ? 1 : 2) /*stderr*/, (uint32_t)buff,
            strlen(buff)};
    asm(
        "mov r0, %[cmd];" "mov r1, %[msg];" "bkpt #0xAB"::
        [cmd]"r"(0x05),[msg]"r"(m):"r0","r1","memory"
    );
}

#endif
