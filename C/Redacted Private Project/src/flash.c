/**
 * \file    flash.c
 * \author  Patrick Lineruth
 * \note    Note I've redacted the date and some file and/or variable names for
 *          personal reasons.
 */

/* ========== Private Includes ============================================== */
#include <dataflash.h>
#include <flash.h>
#include <spi_driver.h>
#include <string.h>

/* ========== Private Variables ============================================= */
static bool _Flash_busy = false, _Flash_init = false;

/* ========== Public Functions ============================================== */
void Flash_init(void)
{
    // Check if flash hasn't been initialised
    if (!_Flash_init)
    {
        // Initialise the SPI Driver
        SPIDriver_Init();

        // Populate the Partial CRC lookup table
        CRC_init();

        // Remember that the flash has been initialised
        _Flash_init = true;
    }
}

void Flash_readWithoutCRC(uint32_t addr, uint8_t *rxBuff, uint32_t len)
{
    _Flash_busy = true;

    for (uint16_t i = 0, l; i < len; i += l)
    {
        // Check how much is left
        l = ((len - i) > 256) ? 256 : len - i;

        // Partial read the Data and store it in the buffer
        dataflashArrayReadLowFreq(addr + i, &rxBuff[i], l);

        // Wait for completion
        dataflashWaitOnReady();
    }

    _Flash_busy = false;
}

bool Flash_readWithCRC(uint32_t addr, uint8_t *rxBuff, uint32_t len, CRC_t crc)
{
    // Create a buffer for the new data
    uint8_t buff[len];

    // Read the data from the flash
    Flash_readWithoutCRC(addr, buff, len);

    // Calculate CRC and check if it's a match
    if (CRC_fast(buff, len) == crc)
    {
        // We got a match, we can now start writing the data to rxBuff
        memcpy(rxBuff, buff, len);
        return (true);
    }
    else
    {
        return (false);
    }
}

bool Flash_readWithCRCIndex(uint32_t addr, uint8_t *rxBuff, uint32_t len,
        uint16_t index)
{
    CRC_t crc;

    // Get the CRC from the CRC Table
    Flash_readWithoutCRC(CRC_TABLE_ADDR + (index * sizeof(CRC_t)),
            (uint8_t*) &crc, sizeof(CRC_t));

    // Read the data and compare the CRC
    return (Flash_readWithCRC(addr, rxBuff, len, crc));
}

void Flash_writeWithoutCRC(uint32_t addr, uint8_t *txBuff, uint32_t len)
{
    _Flash_busy = true;

    for (uint16_t i = 0, l; i < len; i += l)
    {
        // Check how much is left
        l = ((len - i) > 256) ? 256 : len - i;

        // Partial write the data to the flash
        dataflashMemoryProgramThruBuffer1WithErase(addr + i, &txBuff[i], l);

        // Wait for completion
        dataflashWaitOnReady();
    }

    _Flash_busy = false;
}

bool Flash_writeWithCRC(uint32_t addr, uint8_t *txBuff, uint32_t len,
        CRC_t *crc)
{
    // Write the data to the flash
    Flash_writeWithoutCRC(addr, txBuff, len);

    // Create a buffer for the crc check
    uint8_t buff[len];

    // Read what we just wrote
    Flash_readWithoutCRC(addr, buff, len);

    CRC_t _crc = CRC_fast(txBuff, len);

    // Calculate CRC and compare the CRC
    if (_crc == CRC_fast(buff, len))
    {
        *crc = _crc;
        return (true);
    }
    else
    {
        return (false);
    }
}

bool Flash_writeWithCRCIndex(uint32_t addr, uint8_t *txBuff, uint32_t len,
        uint16_t index)
{
    CRC_t crc = 0;

    // Write the data and get the CRC
    if (Flash_writeWithCRC(addr, txBuff, len, &crc))
    {
        // The write was successful, we can now add the CRC to the CRC Table
        Flash_writeWithoutCRC(CRC_TABLE_ADDR + (index * sizeof(CRC_t)),
                (uint8_t*) &crc, sizeof(CRC_t));								// TODO Perhaps an additional check if the CRC was also successfully written
        return (true);
    }
    else
    {
        return (false);
    }
}

bool Flash_busy(void)
{
    return (_Flash_busy);
}

