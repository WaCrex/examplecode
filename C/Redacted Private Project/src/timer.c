/**
 * \file    timer.c
 * \author  Patrick Lineruth
 * \note    Note I've redacted the date and some file and/or variable names in
 *          the for personal reasons.
 */

/* ========== Private Includes ============================================== */
#include "timer.h"
#include "redacted_conf.h"
#include "redacted.h"
#include <stm32f4xx.h>

/* ========== Private Variables ============================================= */
double _Timer_inc = (1000 / __DISPLAY_UPDATE_INTERVAL);
uint32_t _Timer_increment = 0;
Timer_t timer = {0};
bool timerUpdated = false;

/* ========== Public Functions ============================================== */
void Timer_init(void)
{
    RCC_ClocksTypeDef freq;
    // Get the CPU Frequency
    RCC_GetClocksFreq(&freq);
    // Configure and start the SysTick timer
    SysTick_Config(__DISPLAY_UPDATE_INTERVAL * (freq.SYSCLK_Frequency / 1000));
}

const Timer_t Timer_getTime()
{
    // Increment Seconds
    timer.seconds += (_Timer_increment / _Timer_inc);
    _Timer_increment %= (uint16_t) _Timer_inc;
    // Increment Minutes
    timer.minutes += (timer.seconds / 60);
    timer.seconds %= 60;
    // Increment Hours
    timer.hours += (timer.minutes / 60);
    timer.minutes %= 60;
    // Increment Days
    timer.days += (timer.hours / 24);
    timer.hours %= 24;

    return (timer);
}

void SysTick_Handler()
{
    timer.systime++;
    _Timer_increment++;
    timerUpdated = true;
}

