/**
 * \file    config.c
 * \author  Patrick Lineruth
 * \note    There's some bugs in this file that I haven't fixed yet so this file
 *          not exactly ready yet, but I wanted to show the current progress of
 *          it :)
 * 
 * \note    Note I've redacted the date and some file and/or variable names for
 *          personal reasons.
 */

/* ========== Private Includes ============================================== */
#include "config.h"
#include "redacted.h"
#include "flash.h"
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

/* ========== Private Variables ============================================= */
// @formatter:off
const ConfigTable_t config[] =
{
    { // Section 0 - Redacted Configuration
        .address    = 0x00001000,
        .config     = &redaced.config,
        .defaults   = &redaced_defaults,
        .typeSize   = sizeof(RedactedConfig_t),
        .count      = 1
    },
    { // Section 1 - Color Configuration
        .address    = 0x00002000,
        .config     = &redacted.user_colors,
        .defaults   = NULL,
        .typeSize   = sizeof(Color_t),
        .count      = USER_COLORS
    },
    { // Section 2 - Context Configuration
        .address    = 0x00003000,
        .config     = &redacted.str,
        .defaults   = &str_defaults,
        .typeSize   = sizeof(StringObject_t),
        .count      = __DISPLAY_MAX_CONTEXTS
    }
};
// @formatter:on
uint16_t _Config_sections = sizeof(config) / sizeof(ConfigTable_t);
static uint8_t  *_Config_flag;
bool _Config_store = false;

/* ========== Public Functions ============================================== */
void Config_init()
{
    // Initialise the flash
    Flash_init();

    // Reset the master store flag
    _Config_store = false;

    // Count how many configuration fields there are in total
    uint16_t data_count = 0;
    for (uint16_t i = 0; i < _Config_sections; i++)
        data_count += config[i].count;

    // Initialize the store flag array
    _Config_flag = calloc(1, data_count / 8);

    // Iterate through the configuration sections
    for (uint16_t i = 0, crc = 0; i < _Config_sections; i++)
    {
        // Create a pointer for the current section
        const ConfigTable_t *c = &config[i];

        // Iterate through the fields in the configuration section
        for (uint16_t j = 0; j < c->count; j++, crc++)
        {
            // Create a pointer for where the field data should be stored
            uint8_t *output = &((uint8_t*) c->config)[j * c->typeSize];

            // Read the field and check for data corruption
            if (!Flash_readWithCRCIndex(c->address + (j * c->typeSize), output,
                    c->typeSize, crc))
            {
                // The data was corrupted, use defaults instead
                if (c->defaults == NULL)
                    memset(output, 0, c->typeSize);
                else
                    memcpy(output, c->defaults, c->typeSize);

                // Set store flag so what the field is written once done
                _Config_flag[crc / 8] |= (0x80 >> (crc % 8));
                _Config_store = true;
            }
        }
    }

/*
    //TODO  These two lines actually works and the flash is written to, need to look into Config_write()
    //NOTE  Note there's some bugs in this file that I haven't fixed yet so this file not exactly ready
            yet, but I wanted to show the current progress of it :)

    Flash_writeWithCRCIndex(0x00001000, (uint8_t*) &redacted_defaults, sizeof(RedactedConfig_t), 0);
    Flash_writeWithCRCIndex(0x00003000, (uint8_t*) &redacted.str, sizeof(StringObject_t), 1 + USER_COLORS);
*/

    // Store any configuration that needs to be stored
    Config_write();
}


void Config_setFlag(uint16_t section, uint16_t field)
{
    // Make sure section isn't more then the amount of sections
    if (section >= _Config_sections)
        return;

    // Skip to the current section counting every filed
    uint16_t bit = 0;
    for (uint16_t i = 0; i < section; i++)
        bit += config[i].count;

    // Skip to the current field
    bit += field;

    // Set store flag so what the configuration is Config_store() is called.
    _Config_flag[bit / 8] |= (0x80 >> (bit % 8));
}

void Config_store()
{
    _Config_store = true;
}

void Config_write() // TODO Find out what the problem is with this function
{
	// Only continue if there's actually something to write
    if (!_Config_store)
        return;

    uint16_t errCount = 0;

    // Iterate through the configuration sections
    for (uint16_t section = 0, crc = 0; section < _Config_sections; section++)
    {
    	// Create a pointer for the current section
        const ConfigTable_t *c = &config[section];

        // Iterate through the fields in the configuration section
        for (uint16_t field = 0; field < c->count; field++, crc++)
        {
            uint8_t _byte = _Config_flag[crc / 8], _bitmask = (0x80 >> (crc % 8));
            // Check if the field data needs to be written
            if (_byte & _bitmask)
            {
            	// Create a pointer for where the field data should be stored
                uint8_t *output = &((uint8_t*) c->config)[field * c->typeSize];
                // Write configuration to the flash & check for data corruption
                if (!Flash_writeWithCRCIndex(c->address + (field * c->typeSize),
                        output, c->typeSize, crc))
                {
                    // Failed to write the data
                    errCount++;
                }

                // clear store flag
                _Config_flag[crc / 8] &= ~(0x80 >> (crc % 8));
            }
        }
    }

    if (errCount != 0)
    {
        // TODO Alert the user that the configuration couldn't be written and that the flash might be bad
    }

    _Config_store = false;
}
