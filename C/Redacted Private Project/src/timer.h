/**
 * \file    timer.h
 * \author  Patrick Lineruth
 * \note    Note I've redacted the date and some file and/or variable names for
 *          personal reasons.
 */

#ifndef __TIMER_H__
#define __TIMER_H__

/* ========== Public Includes =============================================== */
#include <stdint.h>
#include <stdbool.h>

/* ========== Public Type Definitions ======================================= */
typedef struct Timer_s
{
    uint32_t systime;
    uint16_t days;
    uint8_t hours;
    uint8_t minutes;
    uint8_t seconds;
} Timer_t;

/* ========== Public Variables ============================================== */
//extern Timer_t timer;
extern bool timerUpdated;

/* ========== Public Functions ============================================== */
void Timer_init(void);
const Timer_t Timer_getTime();

#endif//__TIME_H__
