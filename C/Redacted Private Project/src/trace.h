#ifndef __TRACE_H__
#define __TRACE_H__

/**
 * \file    trace.h
 * \author  Patrick Lineruth
 * \note    Note I've redacted the date and some file and/or variable names for
 *          personal reasons.
 */

#ifdef DEBUG
typedef enum {
	TTRACE_INFO		= 0,
	TTRACE_WARN     = 1,
	TTRACE_ERROR	= 2
} TTRACELevel_t;

void TTRACE(TTRACELevel_t level, const char *msg);

//TTRACE
#endif

#endif//__TRACE_H__
