/**
 * \file    config.h
 * \brief   Handles reading/writing of system configuration
 * \author  Patrick Lineruth
 * \note    Note I've redacted the date and some file and/or variable names for
 *          personal reasons.
 */

#ifndef __REDACTED_CONFIG__
#define __REDACTED_CONFIG__

#ifdef __cplusplus
extern "C"
{
#endif

/* ========== Public Includes =============================================== */
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

/* ========== Public Type Definitions ======================================= */
typedef struct ConfigTable_s {
    /**
     * The address on the flash the configuration section is stored at.
     */
    uint32_t address;

    /**
     * Pointer to where the configuration is stored.
     */
    void *config;

    /**
     * Pointer to the default configuration.
     *
     * \note If set to NULL it will just zero-fill the configuration instead.
     */
    const void *defaults;

    /**
     * The size of the data type.
     */
    size_t typeSize;

	/**
	 * The amount of fields in the section (total data size / data type size).
	 */
	uint16_t count;
} ConfigTable_t;

/* ========== Public Functions ============================================== */
/**
 * \brief   Initializes the flash driver and loads the configuration from the\n
 *          flash.
 */
void Config_init(void);

/**
 * \brief   Used to tell that the field has been changed.
 *
 * \note    This function needs to be manually called from the code, every\n
 *          time a field is changed. Otherwise Config_store() won't know what\n
 *          to store.
 *
 * \param [in]  section	    The section of the configuration that was changed
 * \param [in]  field       The field inside the configuration section that\n
 *                          was changed.
 */
void Config_setFlag(uint16_t section, uint16_t field);

/**
 * \brief   Sets config store flag to true, this will trigger a write to the\n
 *          flash next time Config_write() is called from the main loop.
 */
void Config_store(void);

/**
 * \brief   Writes the configuration to the flash.
 *
 * \note    Note that Config_store() needs to be called first.
 */
void Config_write(void);

#ifdef __cplusplus
}
#endif

#endif//__REDACTED_CONFIG__
