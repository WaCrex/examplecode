import sys
import re

# This is the filter we are gonna use to check if our arguments are safe to eval
# -?[\\d]+([.,]\\d+)?  Make sure the first argument is a float
# [+-/*xX]             Make sure the operator is acceptable
# -?[\\d]+([.,]\\d+)?  Make sure the second argument is a float
p = re.compile('-?[\\d]+([.,]\\d+)?\\s[+-/*xX]\\s-?[\\d]+([.,]\\d+)?')

# Make sure we have 3 arguments
if not len(sys.argv) == 4:
    print('Error: You must provide 3 arguments. For example:\n1 + 10\n4 - 8\n9 / 2\n5 x 8')
    exit(1)
else:
    try:
        problem = '{} {} {}'.format(sys.argv[1], sys.argv[2], sys.argv[3])
        # Check if user input is a valid math problem
        if p.match(problem):
            try:
                # Calculate and print the the answer
                print(eval(problem.lower().replace('x', '*')))

                # All done
                exit(0)
            except ZeroDivisionError:
                print('Error: You cannot divide by zero.')
                exit(3)
    except:
        print('Error: \'{}\' is not a valid math problem.'.format(problem))
        exit(2)
