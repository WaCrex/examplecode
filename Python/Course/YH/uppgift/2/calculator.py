import sys

# Make sure we have 3 arguments
if not len(sys.argv) == 4:
    print('Error: You must provide 3 arguments. For example:\n1 + 10\n4 - 8\n9 / 2\n5 x 8')
    exit(1)

# Try to parse the argument as a float
def toFloat(num):
    try:
        return float(num)
    except ValueError:
        print('Error: The argument \'{}\' is not a valid number.'.format(num))
        exit(1)

# Check if the operator is valid
def checkOp(op):
    ops = set('+-/*xX')
    if op in ops:
        return op
    else:
        print('Error: The argument \'{}\' is not a valid operator.'.format(op))
        exit(2)

# Make sure the inputs are safe to use before eval
problem = '{} {} {}'.format(
    toFloat(sys.argv[1]),
    checkOp(sys.argv[2]),
    toFloat(sys.argv[3])
)

try:
    # Calculate and print the the answer
    print(eval(problem.lower().replace('x', '*')))
except ZeroDivisionError:
    print('Error: You cannot divide by zero.')
    exit(3)

# All done
exit(0)

