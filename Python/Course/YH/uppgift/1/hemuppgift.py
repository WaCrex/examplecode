import sys
import os
import os.path

# The default log file
_LOG_FILE = 'transactions.log'

# Black Friday Sales
soldProducts = dict()

def main(logFile = _LOG_FILE):
    # Check if the log file exists
	if not os.path.exists(logFile):
		print('Error: The logfile \'{}\' could not be found.'.format(logFile))
		exit(2)

	# Read the log file
	with open(logFile, 'r') as fp:
		data = fp.readlines()

	# Count the sales
	for line in data:
		product = line.rstrip('\r\n ')
		if product in soldProducts:
			soldProducts[product] += 1
		else:
			soldProducts[product] = 1

	# Print sales count in alphabetical order
	for product, sold in sorted(soldProducts.items()):
		print('{} was sold {} times'.format(product, sold))

	# All done
	exit(0)

# Read command line arguments
if __name__ == '__main__':
	if len(sys.argv) > 2:
		print('Error: You cannot enter more than one argument.')
		exit(1)
	elif len(sys.argv) == 2:
		main(sys.argv[1])
	else:
		main()

