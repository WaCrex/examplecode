import sys


_LOG_FILE = 'potato.log'


def main():
    slogan = 'The best potatoes in town'
    print('Hello, welcome to the potato shop - {}'.format(slogan))

    potatoes = ('King Edward', 'Sötpotatis', 'Bintje', 'Fast')
    name = input('What is your name?: ')
    print('Hello {}, we offer you {} different potatoes: {}'.format(
        name, len(potatoes), potatoes
    ))

    price = 2
    print('The potatoes cost {} each'.format(price))
    discount_min = 10
    print('If you buy {0} or more, pay 1/3 the price. E.g. {0} costs {1:.2f}'.format(
        discount_min, 10*price/3
    ))

    if buy_potatoes():
        amount = potato_amount()
        cost = total_cost(amount, discount_min, price)
        print('Thank you, here are {} potatoes, they cost {:.2f}'.format(
            amount, cost
        ))
        write_log(name, amount, cost)
    else:
        print('Okay :(')
        exit(1)


def buy_potatoes() -> bool:
    while True:
        buy_input = input('Would you like to buy anything? [yes/no]: ').lower()
        if buy_input == 'yes':
            return True
        elif buy_input == 'no':
            return False


def potato_amount(minimum: int = 1, maximum: int = 100) -> int:
    while True:
        try:
            amount = int(input('How many would you like to buy? [{}-{}]: '.format(
                minimum, maximum
            )))
            if amount < minimum or amount > maximum:
                continue
            return amount
        except ValueError:
            pass


def total_cost(amount: int, discount_min: int, price: int) -> float:
    if amount < discount_min:
        return amount*price
    else:
        return amount*price/3


def write_log(name: str, amount: int, cost: float):
    with open(_LOG_FILE, 'a') as f:
        f.write('{} bought {} potatoes for a cost of {:.2f}\n'.format(
            name, amount, cost
        ))


if __name__ == '__main__':
    if len(sys.argv) == 2 and sys.argv[1] == '--log':
        with open(_LOG_FILE, 'r') as f:
            print(f.read())
    else:
        main()
